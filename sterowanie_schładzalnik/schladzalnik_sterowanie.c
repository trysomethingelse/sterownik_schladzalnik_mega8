/*
* sterowanie_sch�adzalnik.cpp
*
* Created: 15.04.2017 17:15:30
*  Author: Mateusz Gutowski
*/


//sterowanie wyswietlaczem i wszystkimi innymi urz�dzeniami odbywa si� za
//pomoc� stanu niskiego
//zegar 8MHz
#include <avr/io.h>
#include <avr/interrupt.h>
#include "ds18b20/ds18b20.h"
#include <util/delay.h>

void setDigitOnLED(int,int);
void showNumber(double ledOut, int ledNo);
int getADCValue(int port);
void temperatureControl(void);

double currentLed = 12.3;
double currentTemperature = 11.1;
double temperatureSet = 5.0;
double timeMixerSet = 30;

int temperatureReadTime = 0;
int mixerTimeSec = 0;
int mixerTimeMin = 0;
int temperatureControlTime = 0;

volatile int presentLedNo = 3;
int cooling = 0;//pomaga w zachowaniu histerezy
int mixing = 0;

//7812.5 impuls�w przypada na 1sekund� przy taktowaniu 8Mhz i prescalerze 1024
volatile int timer0Counter = 0;
//przerobic na odswiezanie wyswietlacza w przerwaniu!!
#define __delayOfDigits 0
//7812/255 ~=31:
#define _TIMER_OVF_SEC 300
//pozwala na od�wie�anie jednej cyfry w oko�o 30Hz przy 3 cyfrach:
#define _TIMER_START_VALUE 200
#define _TURNED_ON 0

#define _TIME_ADC 0
#define _TEMPERATURE_ADC 1

#define _TEMPERATURE_HISTERESIS 1.5
//co tyle sekund temperatura bedzie sprawdzana z po��danymi warto�ciami
#define	_TEMPERATURE_CONTROL_TIME 2
#define _TEMPERATURE_READ_TIME 2
#define _TEMPERATURE_BUTTON (PINC & (1<<PC2))
#define _TEMPERATURE_ANALOG (PINC & (1<<PC0))
#define _TEMPERATURE_READ_ERROR 5
#define _TEMPERATURE_READ_TRIES 5



#define _MIXER_PORT PORTC
#define _MIXER_PIN	PC4
//ile czasu wlaczone:
#define _MIXER_TIME_TURN_ON 2		
#define _MIXER_TIME_BUTTON (PINC & (1<<PC3))
#define _MIXER_TIME_ANALOG (PINC & (1<<PC1))
#define _MIXER_ON (_MIXER_PORT &= ~(1<<_MIXER_PIN))
#define _MIXER_OFF (_MIXER_PORT |= (1<<_MIXER_PIN))
#define _MIXER_TURNED (PINC & (1<<_MIXER_PIN))

#define _COOLER_PORT PORTC
#define	_COOLER_PIN PC5
#define _COOLER_ON (_COOLER_PORT &= ~(1<<_COOLER_PIN))
#define _COOLER_OFF (_COOLER_PORT |= (1<<_COOLER_PIN))
#define _COOLER_TURNED (PINC & (1<<_COOLER_PIN))


ISR(TIMER0_OVF_vect)
{
		timer0Counter++;
		showNumber(currentLed,presentLedNo);
		TCNT0 += _TIMER_START_VALUE;
		
		if(presentLedNo == 1)presentLedNo = 3;
		else presentLedNo--;
		
		//odmierzanie mniej wi�cej jednej sekundy:
		if(timer0Counter >= _TIMER_OVF_SEC)
		{
			timer0Counter = 0;
				
			temperatureReadTime ++;
			temperatureControlTime ++;
			mixerTimeSec ++;
					
		}
		else timer0Counter++;
}
void initialize()
{
	PORTD = 0xFF;		//stan wysoki na wszystkich pinach
	DDRD  = 0xFF;		//wszystkie wyjsciami
	DDRD  &= 0xFE;		// PD0 jako wejscie czujnika temp
	
	PORTC = 0xFF;
	DDRC = 0x30;//dwa ostatnie jako wyj�cia, reszta wej�cia
	
	PORTB = 0xFF;//stan wysoki
	DDRB |= 0b00000111;//trzy pierwsze piny multipleks led, PB2 - 3led, PB1 - 2led, PB0 - 1led
	
	 //Uruchomienie ADC, wewn�trzne napiecie odniesienia, tryb pojedynczej konwersji, preskaler 128, wej�cie PIN5, wynik do prawej
	 ADCSRA =   (1<<ADEN) //ADEN: ADC Enable (uruchomienie przetwornika)
	  //ADPS2:0: ustawienie preskalera, preskaler= 128
	  |(1<<ADPS0)
	  |(1<<ADPS1)
	  |(1<<ADPS2);
	  
	//timer0 8 bitowy:
	TCCR0 |= (1<<CS02) | (1<<CS00); // �r�d�em CLK, preskaler 1024
	TIMSK |= (1<<TOIE0);           //Przerwanie overflow (przepe�nienie timera)
	TCNT0 = _TIMER_START_VALUE;
	sei();
	//ustawianie pocz�tkowych warto�ci z potencjometr�w
	temperatureSet = ((double)getADCValue(_TEMPERATURE_ADC)/100)*3;
	timeMixerSet =  5 + getADCValue(_TIME_ADC)/10;
}
int main(void)
{
	
	initialize();
	while(1)
	{
		/////////////////////////////////////////// OBSLUGA KLAWISZY I WYSWIETLACZA ////////////////////////////////////
		if(_TEMPERATURE_BUTTON == _TURNED_ON)	
		{
			temperatureSet = ((double)getADCValue(_TEMPERATURE_ADC)/100)*3;
			currentLed = temperatureSet;
			_delay_ms(100);
		}
		
		else if(_MIXER_TIME_BUTTON == _TURNED_ON )	
		{
			timeMixerSet = 5 + getADCValue(_TIME_ADC)/10;
			currentLed = timeMixerSet;
			_delay_ms(100);
		}
		else 
		{
			currentLed = currentTemperature;
		}
		/////////////////////////////////////////// KONIEC OBSLUGA KLAWISZY I WYSWIETLACZA ////////////////////////////////////
		/////////////////////////////////////////// SCZYTYWANIE TEMPERATURY ////////////////////////////////////
		if(temperatureReadTime >= _TEMPERATURE_READ_TIME)
		{
			temperatureReadTime = 0;
			double readTemperature = ds18b20_gettemp();
			_delay_ms(750);
			int tries = 0;
			while ((currentTemperature + _TEMPERATURE_READ_ERROR < readTemperature || currentTemperature-_TEMPERATURE_READ_ERROR > readTemperature) && tries < _TEMPERATURE_READ_TRIES )//zbyt du�e skoki odczytu
			{
				readTemperature = ds18b20_gettemp();
				_delay_ms(750);
				tries++;
			}
			currentTemperature = readTemperature;
			
		}
		///////////////////////////////////////////KONIEC SCZYTYWANIE TEMPERATURY ////////////////////////////////////

		///////////////////////////////////////////  MIESZAD�O ////////////////////////////////////
		//liczenie czasu kt�ry up�yn��:
		if(mixerTimeSec >= 60)
		{
			mixerTimeMin ++;
			mixerTimeSec = 0;
			//regularne w��czanie mieszad�a mo�e si� wydarzy� tylko podczas zmiany minut na kolejne:
			if(mixing == 0 && mixerTimeMin >= timeMixerSet)
			{
				mixerTimeMin = 0;
				mixing = 1;
				_MIXER_ON;
			}
		}
		//wy�aczanie mieszadla po ustalonym czasie:
		if((mixerTimeMin >= _MIXER_TIME_TURN_ON) && cooling == 0 && mixing == 1)
		{
			_MIXER_OFF;
			mixerTimeMin = 0;
			mixing = 0;
		}
		///////////////////////////////////////////  KONIEC MIESZADLA ////////////////////////////////////
		/////////////////////////////////////////// AGREGAT ////////////////////////////////////
		if(temperatureControlTime >= _TEMPERATURE_CONTROL_TIME)
		{
			temperatureControlTime = 0;
			temperatureControl();
		}
		///////////////////////////////////////////  KONIEC AGREGAT////////////////////////////////////

	}
	return 0;
}
int getADCValue(int port)
{
	if (port == _TIME_ADC)
	{
		ADMUX  =  (1<<REFS1) | (1<<REFS0) | (1<<MUX0);//ADC1 i 2.56V wewn�trznego napi�cia odniesienia
	}
	else if(port == _TEMPERATURE_ADC)
	{
		ADMUX  =  (1<<REFS1) | (1<<REFS0) ;//ADC0 i 2.56V wewn�trznego napi�cia odniesienia
	}
	ADCSRA |= (1<<ADSC); //ADSC: uruchomienie pojedynczej konwersji  
	while(ADCSRA & (1<<ADSC)); //czeka na zako�czenie konwersji 
	return ADC;
	
}
void temperatureControl(void)
{

	if (cooling == 1 && (currentTemperature <= temperatureSet))
	{
		_COOLER_OFF;
		_MIXER_OFF;
		cooling = 0;
		mixing = 0;
		mixerTimeMin = 0;
		mixerTimeSec = 0;
	}
		
	if(cooling == 0 && (currentTemperature > temperatureSet + _TEMPERATURE_HISTERESIS))
	{
		cooling = 1;
		_COOLER_ON;
		_MIXER_ON;
	}
	
}
void showNumber(double ledOut, int ledNo)//oblicza odpowiedni� cz�� do wy�wietlenia
{
	double ledOutCopy = ledOut;
	
	if(ledOutCopy < 0)ledOutCopy *= -1;
	if (ledOutCopy >= 100) ledOutCopy = 99.9;
	int digit = 0;

	switch(ledNo)
	{
		case 3:
			digit = ledOutCopy/10;
			break;
		case 2:
			digit = (int)ledOutCopy%10;
			break;
		case 1:
			digit = (ledOutCopy - (int)ledOutCopy)*10;
			//digit = (ledOutCopy-front)*10.0;
			break;
	}
	setDigitOnLED(digit,ledNo);
	
	
}
void setDigitOnLED(int digit, int ledNo)
{
	char digitForPD[10]=//dla portu C , jednyka oznacza, kt�re s� aktywne
	{	0b01111110,//0
		0b00001100,//1
		0b10110110,//2
		0b10011110,//3
		0b11001100,//4
		0b11011010,//5
		0b11111010,//6
		0b00001110,//7
		0b11111110,//8
		0b11011110//9
	};
	PORTB |= 0b111;//maska, wy��cza wszystkie tranzysory sterujace led
	switch(ledNo)
	{
		case 1://najmlodszy led, PB0
		PORTB &= 0b11111110;
		break;
		case 2:
		PORTB &= 0b11111101;
		break;
		case 3://najstarszy led, PB3
		PORTB &= 0b11111011;
		break;
	}
	//PORTB = 0b11111110;
	//ustawianie odpowiednich segmentow:
	PORTD = ~digitForPD[digit];
	//_delay_us(__delayOfDigits);
	
}

